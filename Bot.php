<?php
/**
 * Moteur du bot
 */
class Bot
{
    /**
     * le socket pour les fputs 
     * @var Socket
     */
    protected $_socket;
    
    /**
     * Si on répond aux commandes
     * @var boolean
     */
    protected $_acting;
    
    /**
     * Si on continue d'écouter
     * @var boolean 
     */
    protected $_continue;

    /**
     * le signe pour faire appel au bot
     * @var string 
     */
    protected $_charCall = '!';
        
    /**
     * Initialise le socket 
     */
    public function __construct(Socket $socket)
    {
        $this->_socket = $socket;
        
        $this->_acting = true;
        
    }
    
    
    public function start()
    {
        if ($this->_acting) {
            $this->_continue = true;
            
            while ($this->_continue) {
                // read datas
                $donnees = $this->_socket->readSocket();
                $retour = explode(':',$donnees); // On sépare les différentes données.
                // On regarde si c'est un PING, et, le cas échéant, on envoie notre PONG :
                if(rtrim($retour[0]) == 'PING') {
                        $this->_socket->pong($retour[1]);
                }
                if($donnees) {
                    echo $donnees;

                    $commande = explode(' ',$donnees);
                    $message = explode(':',$donnees);

                    // kicked
                    if($commande[1] == 'KICK') {
                        if($commande[3] == $this->_socket->getNick()) {
                            $this->_socket->joinChan();
                        }
                    }

                    // bot is called
                    if(!empty($message[2][0]) && $message[2][0] == $this->_charCall) {

                        // call exists ?
                        $this->_action(trim($message[2]));
                    // try with regex
                    } elseif (isset($message[2])) {
                        $this->_actionRegex(trim($message[2]));
                    }
                }
                // take a breath
                usleep(1000);
            }
        }
    }
    
    /**
     * vérifie si l'action existe ensuite execute ce qui est demandé
     * @param string $actionName 
     */
    protected function _action($actionName)
    {
        $actions = include 'inc.commandes.php';
        
        // commands exists
        if (array_key_exists($actionName, $actions)) {
            // do the job
            $this->_parseAction($actions[$actionName]);
        }
        
        // libère la mémoire
        unset($actions);
    }
    
    /**
     * vérifie si le message rentre dans une regex et agis en conséquence
     */
    protected function _actionRegex($message)
    {
        $regex = include 'inc.regex.php';
        
        foreach ($regex as $pattern => $replace) {
            if (preg_match($pattern, $message)) {
                $message = preg_replace($pattern, $replace, $message);
                $this->_socket->writeMessage($message);
            }
        }
        // libère la mémoire
        unset($regex);
        
    }


    
    /**
     * What to do for every action
     * @param array $queries 
     */
    protected function _parseAction($queries)
    {
        foreach ($queries as $query) {
            list($job, $value) = explode (':', $query, 2);
            
            // method exists DO IT
            if (method_exists($this->_socket, $job)) {
                $this->_socket->{$job}($value);
            }
        }
    }
}