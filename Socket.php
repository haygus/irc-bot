<?php

/**
 * Objet du socket
 */
class Socket
{
    /**
     * server IRC
     * @var string
     */
    protected $_host = 'irc.freenode.org';
    
    /**
     * server's port
     * @var string
     */
    protected $_port = '6667';
    
    /**
     * le chan où se connnecter
     * @var string
     */
    protected $_chan = '#zftalk-fr';
    //protected $_chan = '#centurion-lol';
    //protected $_chan = '#haygus';
    
    /**
     * Bot name
     * @var string
     */
    protected $_nick = 'HaygusBot2';
    
    /**
     * PHP socket
     * @var ressource 
     */
    protected $_socket;
    
    /**
     * timestamp du dernier message pour eviter le flood
     * @var int
     */
    protected $_lastMessage;
    
    /**
     * Connexion lors de l'appel 
     */
    public function __construct()
    {
        $this->_lastMessage = time();
        
        $this->_connect();
        
        $this->_init();
    }
    
    /**
     * Connect
     */
    protected function _connect()
    {
        // Connection to IRC
        $this->_socket = fsockopen($this->_host, $this->_port, $errorNum, $errorString);

        if(!$this->_socket) {
            // Si on n'a pas réussi, on affiche un message d'erreur et on quitte.
            throw new Exception('Impossible de se connecter : '. $errorNum . ' :: ' . $errorString);
        }
    }
    
    /**
     * init stuff
     */
    protected function _init()
    {
        fputs($this->_socket,"USER ".$this->_nick." ".$this->_nick." ".$this->_nick." ".$this->_nick."\r\n");
        // On donne le NICK.
        fputs($this->_socket,"NICK ".$this->_nick."\r\n");

        $this->joinChan();
        
    }
    
    /**
     * write in the socket
     * @param string $string 
     */
    protected function _put($string)
    {
        return fputs($this->_socket, $string);
    }
    
    /**
     * write message into channel
     * @param string $message 
     */
    public function writeMessage($message)
    {
        // last message same second (no flood system)
        if ($this->_lastMessage + 1 >= time()) {
             // wait
            sleep(1);
        }
        $this->_put("PRIVMSG ".$this->_chan." :$message\r\n");
        
        // new last message
        $this->_lastMessage = time();
    }
    
    public function listCommands($include)
    {
        $actions = include $include;
        
        $commands = implode (' ', array_keys($actions));
        
        $this->writeMessage('Commandes disponibles: '.$commands);
        // libère la mémoire
        unset($actions);
    }
    
    /**
     * Join the channel 
     */
    public function joinChan()
    {
        $this->_put("JOIN ".$this->_chan."\r\n");
    }
    
    public function pong($pong)
    {
        $this->_put('PONG :'.$pong);
    }

    /**
     * retourne le chan
     * @return string 
     */
    public function getChan()
    {
        return $this->_chan;
    }
    
    /**
     * retourne le nick
     * @return string 
     */
    public function getNick()
    {
        return $this->_nick;
    }
    
    public function readSocket()
    {
        return fgets($this->_socket, 1024);
    }
}